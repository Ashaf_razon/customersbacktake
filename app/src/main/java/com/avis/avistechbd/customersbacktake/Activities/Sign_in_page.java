package com.avis.avistechbd.customersbacktake.Activities;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.avis.avistechbd.customersbacktake.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Sign_in_page extends AppCompatActivity {

    @BindView(R.id.facebook_button)
    Button facebookButton;
    @BindView(R.id.getMailPhone)
    EditText getMailPhone;
    @BindView(R.id.forgotPassClick)
    TextView forgotPassClick;
    @BindView(R.id.getPassword)
    EditText getPassword;
    @BindView(R.id.submitValues)
    Button submitValues;
    @BindView(R.id.createNew)
    TextView createNew;

    private Handler handler;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Sign in");
        getSupportActionBar().hide();
        setContentView(R.layout.activity_sign_in_page);
        ButterKnife.bind(this);

        handler = new Handler(Looper.getMainLooper());
        pd = new ProgressDialog(Sign_in_page.this);
        pd.setIndeterminate(true);
        pd.setMessage("Loading");
        pd.setCancelable(false);
    }

    @OnClick({R.id.facebook_button, R.id.forgotPassClick, R.id.submitValues, R.id.createNew})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.facebook_button:
                Toast.makeText(Sign_in_page.this,"Facebook log in",Toast.LENGTH_LONG).show();
                break;
            case R.id.forgotPassClick:
                Intent goForgotPass = new Intent(Sign_in_page.this,ForGotPass.class);
                carriedIntentMethod(goForgotPass);
                break;
            case R.id.submitValues:
                Intent goHomePage = new Intent(Sign_in_page.this,HomePage.class);
                carriedIntentMethod(goHomePage);
                break;
            case R.id.createNew:
                Intent goCreateNew = new Intent(Sign_in_page.this,PhoneAuth.class);
                carriedIntentMethod(goCreateNew);
                break;
        }
    }

    private void carriedIntentMethod(final Intent takeIntentRef) {
        pd.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            pd.cancel();
                            startActivity(takeIntentRef);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        }).start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }
}
