package com.avis.avistechbd.customersbacktake.Activities;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.avis.avistechbd.customersbacktake.R;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class Support_act extends Fragment {
    private Handler handler;
    private Context context;
    private ProgressDialog pd;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Support");
        View view = inflater.inflate(R.layout.activity_support_act, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        handler = new Handler(Looper.getMainLooper());
        pd = new ProgressDialog(context);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.setIndeterminate(true);
        return view;
    }

    @OnClick(R.id.support_call)
    public void onSupportCallClicked() {
        callClickResponse();
    }

    @OnClick(R.id.support_mail)
    public void onSupportMailClicked() {
        callClickResponse();
    }

    @OnClick(R.id.support_web)
    public void onSupportWebClicked() {
        callClickResponse();
    }

    @OnClick(R.id.support_emergency)
    public void onSupportEmergencyClicked() {
        callClickResponse();
    }

    @OnClick(R.id.support_complaint)
    public void onSupportComplaintClicked() {
        callClickResponse();
    }

    @OnClick(R.id.support_faq)
    public void onSupportFaqClicked() {
        Intent goFaq = new Intent(context, Faq_quick.class);
        carriedIntentMethod(goFaq);
    }

    private void callClickResponse(){
        Toast.makeText(context,"Clicked",Toast.LENGTH_LONG).show();
    }

    private void carriedIntentMethod(final Intent takeIntentRef) {
        pd.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            pd.cancel();
                            startActivity(takeIntentRef);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        }).start();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
