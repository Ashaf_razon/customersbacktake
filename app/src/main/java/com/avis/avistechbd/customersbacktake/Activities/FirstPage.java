package com.avis.avistechbd.customersbacktake.Activities;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.avis.avistechbd.customersbacktake.R;
import com.avis.avistechbd.customersbacktake.view_pages.mDialogClass;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import butterknife.ButterKnife;
import butterknife.OnClick;
public class FirstPage extends AppCompatActivity {
    private Handler handler;
    private Context context;
    private ProgressDialog pd;
    private CarouselView carouselView;

    private int[] sampleImages = {R.drawable.apple_watch,R.drawable.watch,R.drawable.camera,R.drawable.camera_lens,R.drawable.i_phone_x,R.drawable.mac_book,R.drawable.printer};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_page);
        //getSupportActionBar().hide();
        setTitle(R.string.title_app);
        ButterKnife.bind(this);
        handler = new Handler(Looper.getMainLooper());
        context = FirstPage.this;
        pd = new ProgressDialog(FirstPage.this);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.setIndeterminate(true);

        carouselView = (CarouselView) findViewById(R.id.carouselView);
        carouselView.setPageCount(sampleImages.length);
        carouselView.setImageListener(imageListener);
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(sampleImages[position]);
        }
    };

    @OnClick({R.id.u_sign_in, R.id.u_sign_up})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.u_sign_in:
                Intent goDrawer = new Intent(FirstPage.this,Sign_in_page.class);
                carriedIntentMethod(goDrawer);
                break;
            case R.id.u_sign_up:
                try{
                    Intent goPhoneAuth = new Intent(FirstPage.this, PhoneAuth.class);
                    carriedIntentMethod(goPhoneAuth);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        mDialogClass myDialog = new mDialogClass(FirstPage.this,R.string.dialog_title,1);
        try{
            myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }catch (Exception e){
            e.printStackTrace();
        }
        myDialog.setCancelable(false);
        myDialog.show();
    }

    private void carriedIntentMethod(final Intent takeIntentRef) {
        pd.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            pd.cancel();
                            startActivity(takeIntentRef);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        }).start();
    }

}
