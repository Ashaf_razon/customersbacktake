package com.avis.avistechbd.customersbacktake.view_pages;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.avis.avistechbd.customersbacktake.R;

public class CustomDialogTwo extends Dialog implements View.OnClickListener {

    public Activity activity;
    public Dialog d;
    public Button yes;
    TextView txtDia;
    String dialog_title;
    int flagNo;

    public CustomDialogTwo(Activity activity, String dialog_title,int flagNo) {
        super(activity);
        this.activity = activity;
        this.dialog_title = dialog_title;
        this.flagNo = flagNo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog_two);
        yes = (Button) findViewById(R.id.btn_yes);
        txtDia = (TextView) findViewById(R.id.txt_dia);
        txtDia.setText(dialog_title);
        yes.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_yes:
//                activity.finish();
                break;
            default:
                break;
        }
        dismiss();
    }
}
