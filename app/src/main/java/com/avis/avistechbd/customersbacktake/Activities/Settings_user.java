package com.avis.avistechbd.customersbacktake.Activities;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.avis.avistechbd.customersbacktake.R;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class Settings_user extends Fragment {

    private Unbinder unbinder;
    private Context context;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Settings");
        View view = inflater.inflate(R.layout.activity_settings_user, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void callClickResponse(String showText){
        Toast.makeText(context,showText,Toast.LENGTH_LONG).show();
    }

    @OnClick({R.id.settCurrency, R.id.settCurrencyTaka, R.id.settSite, R.id.settSeiteView, R.id.settChPass, R.id.settPrPo})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.settCurrency:
                callClickResponse("Currency BDT");
                break;
            case R.id.settCurrencyTaka:
                callClickResponse("Currency BDT");
                break;
            case R.id.settSite:
                callClickResponse("www.avistechbd.com");
                break;
            case R.id.settSeiteView:
                callClickResponse("www.avistechbd.com");
                break;
            case R.id.settChPass:
                callClickResponse("Change your password");
                break;
            case R.id.settPrPo:
                callClickResponse("Please read Privacy & Policy");
                break;
        }
    }
}
