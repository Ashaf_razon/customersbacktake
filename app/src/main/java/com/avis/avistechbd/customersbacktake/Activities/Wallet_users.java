package com.avis.avistechbd.customersbacktake.Activities;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.avis.avistechbd.customersbacktake.R;
import com.avis.avistechbd.customersbacktake.view_pages.CustomDialogTwo;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class Wallet_users extends Fragment {
    private Handler handler;
    private Context context;
    private ProgressDialog pd;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("My Wallet");
        View view = inflater.inflate(R.layout.activity_wallet_users, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getActivity();
        handler = new Handler(Looper.getMainLooper());
        pd = new ProgressDialog(context);
        pd.setMessage("Loading...");
        pd.setCancelable(false);
        pd.setIndeterminate(true);
        return view;
    }

    @OnClick({R.id.walletBonus, R.id.walletAvisBonus, R.id.walletCreditDebit, R.id.walletPocket, R.id.walletPayHistory})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.walletBonus:
                myDialog("Wallet Bonus");
                break;
            case R.id.walletAvisBonus:
                myDialog("Avis Special Bonus");
                break;
            case R.id.walletCreditDebit:
                callClickResponse("Credit / Debit Acc.");
                break;
            case R.id.walletPocket:
                myDialog("My Pocket");
                break;
            case R.id.walletPayHistory:
                callClickResponse("Your payment history");
                break;
        }
    }

    private void myDialog(String dialogTitle){
        CustomDialogTwo myDialog = new CustomDialogTwo((Activity) context,dialogTitle,1);
        try{
            myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }catch (Exception e){
            e.printStackTrace();
        }
        myDialog.setCancelable(false);
        myDialog.show();
    }
    private void callClickResponse(String showText){
        Toast.makeText(context,showText,Toast.LENGTH_LONG).show();
    }
    private void carriedIntentMethod(final Intent takeIntentRef) {
        pd.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            pd.cancel();
                            startActivity(takeIntentRef);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        }).start();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
