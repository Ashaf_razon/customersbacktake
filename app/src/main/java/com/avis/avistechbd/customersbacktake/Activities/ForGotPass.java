package com.avis.avistechbd.customersbacktake.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.avis.avistechbd.customersbacktake.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForGotPass extends AppCompatActivity {

    @BindView(R.id.setNewPass_1)
    EditText setNewPass1;
    @BindView(R.id.setNewPass_2)
    EditText setNewPass2;
    @BindView(R.id.resetButton)
    Button resetButton;

    private ProgressDialog pd;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("FORGOT PASSWORD");
        setContentView(R.layout.activity_for_got_pass);
        ButterKnife.bind(this);
        handler = new Handler(Looper.getMainLooper());
        pd = new ProgressDialog(ForGotPass.this);
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        pd.setMessage("Loading...");
    }

    @OnClick(R.id.resetButton)
    public void onViewClicked() {
        Toast.makeText(ForGotPass.this,"Pass successfully changed, Thank you",Toast.LENGTH_LONG).show();
    }

    private void carriedIntentMethod(final Intent takeIntentRef) {
        pd.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            pd.cancel();
                            startActivity(takeIntentRef);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        }).start();
    }
}
