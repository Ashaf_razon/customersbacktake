package com.avis.avistechbd.customersbacktake.Activities;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import com.avis.avistechbd.customersbacktake.R;
import com.avis.avistechbd.customersbacktake.view_pages.mDialogClass;

public class HomePage extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private NavigationView navigationView;
    private Menu getNavMenu;
    private Handler handler;
    private ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Home");
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        getNavMenu = navigationView.getMenu();
        disableNavigationViewScrollbars(navigationView); //disable scrollbar
        handler = new Handler(Looper.getMainLooper());
        pd = new ProgressDialog(HomePage.this);
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        pd.setMessage("Loading...");
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
            showLogoutMessage(3);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.request_item) {
            carriedIntentMethod(1);
        } else if (id == R.id.my_orders) {
            carriedIntentMethod(2);
        }else if (id == R.id.get_notif) {
            carriedIntentMethod(3);
        } else if (id == R.id.chat_box) {
            carriedIntentMethod(4);
        } else if (id == R.id.your_wallet) {
            carriedIntentMethod(5);
        } else if (id == R.id.settings_set) {
            carriedIntentMethod(6);
        } else if (id == R.id.get_users_help) {
            carriedIntentMethod(7);
        }else if (id == R.id.get_log_out) {
            showLogoutMessage(2);
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showLogoutMessage(int setFlagNo) {
        mDialogClass myDialog = new mDialogClass(HomePage.this, R.string.dialog_title_2,setFlagNo);
        try{
            myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }catch (Exception e){
            e.printStackTrace();
        }
        myDialog.setCancelable(true);
        myDialog.show();
    }

    private void carriedIntentMethod(final int setId) {
        pd.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            switch(setId){
                                case 1:
                                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Request_new()).commit();
                                    break;
                                case 2:
                                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new OrderList_users()).commit();
                                    break;
                                case 3:
                                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Notifications()).commit();
                                    break;
                                case 4:
                                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Message_box()).commit();
                                    break;
                                case 5:
                                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Wallet_users()).commit();
                                    break;
                                case 6:
                                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Settings_user()).commit();
                                    break;
                                case 7:
                                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Support_act()).commit();
                                    break;

                            }
                            pd.cancel();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        }).start();
    }

    private void disableNavigationViewScrollbars(NavigationView navigationView) {
        if (navigationView != null) {
            NavigationMenuView navigationMenuView = (NavigationMenuView) navigationView.getChildAt(0);
            if (navigationMenuView != null) {
                navigationMenuView.setVerticalScrollBarEnabled(false);
            }
        }
    }

}
