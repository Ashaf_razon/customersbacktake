package com.avis.avistechbd.customersbacktake.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.avis.avistechbd.customersbacktake.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Registration_sheet extends AppCompatActivity {

    @BindView(R.id.enterName)
    EditText enterName;
    @BindView(R.id.enterPhone)
    EditText enterPhone;
    @BindView(R.id.enterDiv)
    EditText enterDiv;
    @BindView(R.id.enterFullAdd)
    EditText enterFullAdd;
    @BindView(R.id.enterPostalCode)
    EditText enterPostalCode;
    @BindView(R.id.saveDefaultAdd)
    Switch saveDefaultAdd;
    @BindView(R.id.saveUsersInfo)
    Button saveUsersInfo;

    private Handler handler;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Registration");
        setContentView(R.layout.activity_registration_sheet);
        ButterKnife.bind(this);
        handler = new Handler(Looper.getMainLooper());
        pd = new ProgressDialog(Registration_sheet.this);
        pd.setIndeterminate(true);
        pd.setMessage("Loading");
        pd.setCancelable(false);
    }

    @OnClick({R.id.saveDefaultAdd, R.id.saveUsersInfo})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.saveDefaultAdd:
                break;
            case R.id.saveUsersInfo:
                Intent goSignIn = new Intent(Registration_sheet.this,Sign_in_page.class);
                carriedIntentMethod(goSignIn);
                break;
        }
    }

    private void carriedIntentMethod(final Intent takeIntentRef) {
        pd.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            pd.cancel();
                            Toast.makeText(Registration_sheet.this,"Info saved successfully, Please sign in now",Toast.LENGTH_LONG).show();
                            startActivity(takeIntentRef);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        }).start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }
}
