package com.avis.avistechbd.customersbacktake.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.avis.avistechbd.customersbacktake.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PhoneAuth extends AppCompatActivity {

    @BindView(R.id.enterPhoneAuth)
    EditText enterPhoneAuth;
    @BindView(R.id.varifyPhonAuth)
    Button varifyPhonAuth;
    @BindView(R.id.codeSendInPhnText)
    TextView codeSendInPhnText;
    @BindView(R.id.getAuthCode_1)
    EditText getAuthCode1;
    @BindView(R.id.getAuthCode_2)
    EditText getAuthCode2;
    @BindView(R.id.getAuthCode_3)
    EditText getAuthCode3;
    @BindView(R.id.getAuthCode_4)
    EditText getAuthCode4;
    @BindView(R.id.getAuthCode_5)
    EditText getAuthCode5;
    @BindView(R.id.getAuthCode_6)
    EditText getAuthCode6;
    @BindView(R.id.submit_code)
    Button submitCode;

    private String getUphn;
    //Authentication
    private ProgressDialog pd;
    private String sentCode;
    //UsersPhoneSave
    private SharedPreferences mySharedPref;
    private SharedPreferences.Editor editSave;
    private String myRef = "myCustPhone"; //Reference_Key
    private String myPhnKey = "custPhn";
    //FireDB_instance
    private String getFlagsAct;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_phone_auth);
        ButterKnife.bind(this);
        initPrimarysettings();
        new Thread(new Runnable() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            textAutoShift();
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        }).start();
    }

    @OnClick({R.id.varifyPhonAuth, R.id.submit_code})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.varifyPhonAuth:
                callVarificationMethode();
                break;
            case R.id.submit_code:
                Intent goUsersInfo = new Intent(PhoneAuth.this,Registration_sheet.class);
                carriedIntentMethod(goUsersInfo);
                break;
        }
    }

    private void callVarificationMethode() {
        pd.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    Thread.sleep(500);
                }catch (Exception e){
                    e.printStackTrace();
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        pd.cancel();
                        codeSendInPhnText.setText(R.string.codeSendMsg);
                    }
                });
            }
        }).start();
    }

    private void textAutoShift() {
        getAuthCode1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(getAuthCode1.getText().toString().length()== 1){
                    getAuthCode1.clearFocus();
                    getAuthCode2.requestFocus();
                    getAuthCode2.setCursorVisible(true);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        getAuthCode2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(getAuthCode2.getText().toString().length() == 1){
                    getAuthCode2.clearFocus();
                    getAuthCode3.requestFocus();
                    getAuthCode3.setCursorVisible(true);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        getAuthCode3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(getAuthCode3.getText().toString().length() == 1){
                    getAuthCode3.clearFocus();
                    getAuthCode4.requestFocus();
                    getAuthCode4.setCursorVisible(true);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        getAuthCode4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(getAuthCode4.getText().toString().length() == 1){
                    getAuthCode4.clearFocus();
                    getAuthCode5.requestFocus();
                    getAuthCode5.setCursorVisible(true);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        getAuthCode5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(getAuthCode5.getText().toString().length() == 1){
                    getAuthCode5.clearFocus();
                    getAuthCode6.requestFocus();
                    getAuthCode6.setCursorVisible(true);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void initPrimarysettings() {
        try{
            handler = new Handler(Looper.getMainLooper());
            getFlagsAct = getIntent().getStringExtra("debdash");
            mySharedPref = getSharedPreferences(myRef, Context.MODE_PRIVATE);
            pd = new ProgressDialog(PhoneAuth.this);
            pd.setMessage("Loading...");
            pd.setCancelable(false);
            pd.setIndeterminate(true);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void carriedIntentMethod(final Intent takeIntentRef) {
        pd.show();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        try{
                            pd.cancel();
                            startActivity(takeIntentRef);
                        }catch (Exception e){
                            Log.d("TAG",e.getMessage());
                        }
                    }
                });
            }
        }).start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        finish();
    }
}
